module gitlab.com/transnano/pagerduty-api

go 1.15

require (
	github.com/gin-gonic/gin v1.6.3
	github.com/json-iterator/go v1.1.10 // indirect
	github.com/prometheus/client_golang v1.7.1
	github.com/stretchr/testify v1.6.1 // indirect
)
